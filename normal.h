#ifndef SRC_NORMAL_H
#define SRC_NORMAL_H

#include <pthread.h>
#include <sys/time.h>


#define TEST_KEY_PRESS 0x01
#define TEST_KEY_RELESE 0xc1
#define TEST_KEY_RELESE1 0x81

#define LIGHT_KEY_PRESS 0x02
#define LIGHT_KEY_RELESE 0x82
#define LIGHT_KEY_RELESE1 0xc2

#define OPTION_KEY_PRESS 0x04
#define OPTION_KEY_RELESE 0x84

#define MP4_FILE 0
#define AVI_FILE 1

enum PWM_STATUS{
	PWM_ZERO,
	PWM_HALF,
	PWM_FULL
};

enum PLAY_MODE{
	PLAY_SYNCHRONIZE_MODE,
	PLAY_KEY_MODE
};

enum NORMAL_STATUS{
	FALSE_,
	TRUE_
};

enum SWITCH_MODE{
	SWITCH_INIT,
	SWITCH_PRESS,
	SWITCH_RELESE,
	SWITCH_LONG
};

struct DispatcherGather{
	enum PWM_STATUS pwm_status_;
	enum PLAY_MODE play_mode_;
	enum NORMAL_STATUS synchronize_status;
	enum NORMAL_STATUS key_status;
	enum NORMAL_STATUS store_is_full;
	enum NORMAL_STATUS already_play_;
	unsigned char already_synch;
	enum NORMAL_STATUS switch_key_sync;
	struct timeval time_start[3];
	unsigned save_file_mode;
};

typedef struct DispatcherGather DispatcherGather;


int analyze_currentpath_maxid(char* path);
int analyze_avipath_maxid(char* path);

float collect_disk_store_info();
DispatcherGather* get_dispatch_pointer();
void usb_update_mp4_file();

#endif













