#include <linux/module.h>
#include <linux/version.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/sysctl.h>
#include <linux/uaccess.h>
#include <linux/miscdevice.h>
#include <linux/io.h>
#include <linux/spinlock.h>
#include <linux/timer.h>
#include <linux/irq.h>
#include <linux/poll.h>
#include <linux/wait.h>
#include <linux/sched.h>
#include <linux/interrupt.h>

#include "hi3518e-gpio.h"

/* Key_val description of each bit:
 * Support up to 8 keys
 *
 * 31 30 	29 28 	27 26 	25 24 	23 22 	21 20 	19 18 	17 16
 * KEY_7    KEY_6   KEY_5   KEY_4   KEY_3   KEY_2   KEY_1   KEY_0
 * Take KEY0 as an example, 
 * BIT_17: Mark whether the current button is pressed or released
 * 0 : pressed, 1 : released
 * BIT_16: Mark whether the current button is long or short press
 * 0 : short press, 1 : long press
 *
 * 15 14 13 12 11 10 9 8 
 * Reserved
 *
 * 7        6       5       4       3       2       1       0
 * KEY_7    KEY_6   KEY_5   KEY_4   KEY_3   KEY_2   KEY_1   KEY_0
 * Mark if the current button has an action
 * 0 : no action, 1 : have action
 */

struct hi3518e_key_desc{
	struct hi3518e_gpio_desc gpio_desc;
	struct timespec press_time;
	struct timespec release_time;
	unsigned int pinval;
};

static wait_queue_head_t select_wait;

static DEFINE_SPINLOCK(hi3518e_key_lock);
static DECLARE_WAIT_QUEUE_HEAD(hi3518e_key_wait_queue);
static DECLARE_WAIT_QUEUE_HEAD(select_wait);


static volatile int isInterrupt = 0;
static unsigned int key_val;
static unsigned int key_Press = 0;

static struct hi3518e_key_desc keys_desc[] = {
	{{7, 1, IRQ_TYPE_EDGE_BOTH, 0}, {0,0},  {0,0}, 0},
	{{6, 6, IRQ_TYPE_EDGE_BOTH, 1}, {0,0},  {0,0}, 0},
	{{6, 7, IRQ_TYPE_EDGE_BOTH, 2}, {0,0},  {0,0}, 0},
};

#define KEY_NUM_SIZEOF	(sizeof(keys_desc) / sizeof(keys_desc[0]))

static int hi3518e_gpio_direction_output(unsigned int bank, unsigned int pin)
{
	unsigned long flags;
	int val;

    spin_lock_irqsave(&hi3518e_key_lock, flags);
    
    val = readl(GPIO_DIR(bank));
    val |= 1 << pin;
    writel(val, GPIO_DIR(bank));
	
    spin_unlock_irqrestore(&hi3518e_key_lock, flags);

	return 0;
}

static int hi3518e_gpio_direction_input(unsigned int bank, unsigned int pin)
{
	unsigned long flags;
	int val;

    spin_lock_irqsave(&hi3518e_key_lock, flags);
    
    val = readl(GPIO_DIR(bank));
    val &= ~(1 << pin);
    writel(val, GPIO_DIR(bank));
	
    spin_unlock_irqrestore(&hi3518e_key_lock, flags);

	return 0;
}

static int  hi3518e_gpio_set(unsigned int bank, unsigned int pin, unsigned value)
{
	unsigned long flags;
	unsigned int val = 0;

	spin_lock_irqsave(&hi3518e_key_lock, flags);

	if(value)
		val = 1 << pin;
	else
		val = 0;
    
    writel(val, GPIO_DATA(bank, pin));

	spin_unlock_irqrestore(&hi3518e_key_lock, flags);
		
	return 0;
}

static int hi3518e_gpio_get(unsigned int bank, unsigned int pin)
{
	unsigned int val = 0;

    val = readl(GPIO_DATA(bank, pin));
	
	return val >> pin;
}

/* Some special pins are not GPIO mode by default. */
static int hi318e_set_muxctrl_to_gpio_mode(unsigned int bank, unsigned int pin)
{
	unsigned long flags;

	spin_lock_irqsave(&hi3518e_key_lock, flags);

	if(bank == 0 && pin == 5)
		writel(0x01, MUXCTRL_REG1);
	if(bank == 5 && pin == 4)
		writel(0x02, MUXCTRL_REG44);
	if(bank == 7 && pin == 2)
		writel(0x01, MUXCTRL_REG58);
	if(bank == 7 && pin == 3)
		writel(0x01, MUXCTRL_REG59);
	if(bank == 7 && pin == 4)
		writel(0x01, MUXCTRL_REG60);
	if(bank == 7 && pin == 5)
		writel(0x01, MUXCTRL_REG61);
	if(bank == 7 && pin == 6)
		writel(0x01, MUXCTRL_REG62);
	if(bank == 7 && pin == 7)
		writel(0x01, MUXCTRL_REG63);
	if(bank == 8 && pin == 0)
		writel(0x01, MUXCTRL_REG64);
	if(bank == 8 && pin == 1)
		writel(0x01, MUXCTRL_REG65);

	spin_unlock_irqrestore(&hi3518e_key_lock, flags);
	
	return 0;
}

static int hi3518e_gpio_interrupt_clear(unsigned int bank, unsigned int pin)
{
	unsigned long flags;
	unsigned int val = 0;

	spin_lock_irqsave(&hi3518e_key_lock, flags);
	
	val = readl(GPIO_IC(bank));
	val |= 1 << pin;
	writel(val, GPIO_IC(bank)); 

	spin_unlock_irqrestore(&hi3518e_key_lock, flags);

	return 0;
}

static int hi3518e_gpio_interrupt_enable(unsigned int bank, unsigned int pin)
{
	unsigned long flags;
	unsigned int val = 0;

	spin_lock_irqsave(&hi3518e_key_lock, flags);
	
	val = readl(GPIO_IE(bank));
	val |= 1 << pin;
	writel(val, GPIO_IE(bank)); 

	spin_unlock_irqrestore(&hi3518e_key_lock, flags);
	
	return 0;
}

static int hi3518e_gpio_interrupt_disable(unsigned int bank, unsigned int pin)
{
	unsigned long flags;
	unsigned int val = 0;

	spin_lock_irqsave(&hi3518e_key_lock, flags);
	
	val = readl(GPIO_IE(bank));
	val &= ~(1 << pin);
	writel(val, GPIO_IE(bank)); 

	spin_unlock_irqrestore(&hi3518e_key_lock, flags);
	
	return 0;
}

static int hi3518e_gpio_interrupt_masked_status(unsigned int bank, unsigned int pin)
{
	unsigned int val = 0;
	
	val = readl(GPIO_MIS(bank));
	
	return (val >> pin) & 0x01;
}

static int hi3518e_gpio_raw_interrupt_status(unsigned int bank, unsigned int pin)
{
	unsigned int val = 0;
	
	val = readl(GPIO_RIS(bank));
	return (val >> pin) & 0x01;
}

static int hi3518e_gpio_interrupt_config(unsigned int bank, 
											unsigned int pin,unsigned int irq_type)
{
	unsigned long flags;
	unsigned int val = 0;

	spin_lock_irqsave(&hi3518e_key_lock, flags);

	switch(irq_type){
		case IRQ_TYPE_EDGE_RISING:
			/* edge-sensitive mode */
			val = readl(GPIO_IS(bank));
			val &= ~(1 << pin);
			writel(val, GPIO_IS(bank));
			/* single-edge-sensitive mode */
			val = readl(GPIO_IBE(bank));
			val &= ~(1 << pin);
			writel(val, GPIO_IBE(bank));
			/* rising-edge-sensitive mode */
			val = readl(GPIO_IEV(bank));
			val |= 1 << pin;
			writel(val, GPIO_IEV(bank));
			/* clear the interrupts */
			hi3518e_gpio_interrupt_clear(bank, pin);		
			break;
			
		case IRQ_TYPE_EDGE_FALLING:
			/* edge-sensitive mode */
			val = readl(GPIO_IS(bank));
			val &= ~(1 << pin);
			writel(val, GPIO_IS(bank));
			/* single-edge-sensitive mode */
			val = readl(GPIO_IBE(bank));
			val &= ~(1 << pin);
			writel(val, GPIO_IBE(bank));
			/* falling-edge-sensitive mode */
			val = readl(GPIO_IEV(bank));
			val &= ~(1 << pin);
			writel(val, GPIO_IEV(bank));
			/* clear the interrupts */
			hi3518e_gpio_interrupt_clear(bank, pin);
			break;
			
		case IRQ_TYPE_EDGE_BOTH:
			/* edge-sensitive mode */
			val = readl(GPIO_IS(bank));
			val &= ~(1 << pin);
			writel(val, GPIO_IS(bank));
			/* dual-edge-sensitive mode */
			val = readl(GPIO_IBE(bank));
			val |= 1 << pin;
			writel(val, GPIO_IBE(bank));
			/* clear the interrupts */
			hi3518e_gpio_interrupt_clear(bank, pin);
			break;
			
		case IRQ_TYPE_LEVEL_HIGH:
			/* level-sensitive mode */
			val = readl(GPIO_IS(bank));
			val |= 1 << pin;
			writel(val, GPIO_IS(bank));
			/* high-level-sensitive mode */
			val = readl(GPIO_IEV(bank));
			val |= 1 << pin;
			writel(val, GPIO_IEV(bank));
			/* clear the interrupts */
			hi3518e_gpio_interrupt_clear(bank, pin);
			break;
			
		case IRQ_TYPE_LEVEL_LOW:
			/* level-sensitive mode */
			val = readl(GPIO_IS(bank));
			val |= 1 << pin;
			writel(val, GPIO_IS(bank));
			/* low-level-sensitive mode */
			val = readl(GPIO_IEV(bank));
			val &= ~(1 << pin);
			writel(val, GPIO_IEV(bank));
			/* clear the interrupts */
			hi3518e_gpio_interrupt_clear(bank, pin);
			break;

		default:
			break;		
	}

	spin_unlock_irqrestore(&hi3518e_key_lock, flags);

	return 0;
}

static irqreturn_t hi3518e_key_irq_handler(int irqno, void *dev)
{
	int i;
	key_val &= 0x3f;
	for(i = 0;i < KEY_NUM_SIZEOF; i++){
		if(hi3518e_gpio_interrupt_masked_status(keys_desc[i].gpio_desc.bank, keys_desc[i].gpio_desc.pin)){
			hi3518e_gpio_interrupt_clear(keys_desc[i].gpio_desc.bank,keys_desc[i].gpio_desc.pin);
			keys_desc[i].pinval = hi3518e_gpio_get(keys_desc[i].gpio_desc.bank, keys_desc[i].gpio_desc.pin); 	
			if(keys_desc[i].pinval){                             //release
				key_val |=	1 << keys_desc[i].gpio_desc.flag;
			}
			else{                                                 //press
				key_val &=	~(1 << keys_desc[i].gpio_desc.flag);
			}
			key_val |= 1 << (keys_desc[i].gpio_desc.flag + 4);
			printk("bank:%d,pin:%d,flag:%d,pinval:%d，key:%d\n\r",keys_desc[i].gpio_desc.bank,keys_desc[i].gpio_desc.pin,keys_desc[i].gpio_desc.flag,keys_desc[i].pinval,key_val);
		}
	}
	isInterrupt = 1;
	wake_up_interruptible(&hi3518e_key_wait_queue);
	
	return IRQ_HANDLED;
}

int key_main_thread(void *args){
	int val = 0;	
	while(1){
		val = wait_event_interruptible_timeout(select_wait, key_Press, 2 * HZ);
		if(val > 0){
			printk("key press long time\n\r");
		}
		else if(val == 0){
			printk("time out\n\r");
		}
		key_Press = 0;
		
	}
}

static int hi3518e_key_open(struct inode *inode, struct file *filep)
{
	return 0;
}

static ssize_t hi3518e_key_read(struct file *file, char __user *buf, size_t size, loff_t *ppos)
{	
	if (file->f_flags & O_NONBLOCK){
		if (!isInterrupt)
			return -EAGAIN;
	}
	else{
		wait_event_interruptible(hi3518e_key_wait_queue, isInterrupt);
	}

	copy_to_user(buf, &key_val, sizeof(key_val));
	isInterrupt = 0;

	return 1;
}

static int hi3518e_key_close(struct inode *inode, struct file *filep)
{
	return 0;
}

static unsigned int hi3518e_key_poll(struct file *filep, struct  poll_table_struct *wait)
{
	unsigned int mask;

	poll_wait(filep, &hi3518e_key_wait_queue, wait); 

	if (isInterrupt)
		mask |= POLLIN | POLLRDNORM;

	return mask;
}


static struct file_operations hi3518e_key_fops = {
	.owner = THIS_MODULE,
	.open  = hi3518e_key_open,
	.read = hi3518e_key_read,
	.release = hi3518e_key_close,
	.poll = hi3518e_key_poll,
};

static struct miscdevice hi3518e_key_dev = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "hi3518e-key",
	.fops = &hi3518e_key_fops
};

static int __init hi3518e_key_init(void)
{
	int ret;
	int i;
	//init_timer(&hi3518e_key_timer);
	//hi3518e_key_timer.function = hi3518e_key_timer_timer_function;
	
	ret = misc_register(&hi3518e_key_dev);
	if (ret != 0) {
		pr_err("failed to register the hi3518e key \n");
		return ret;
	}

	for(i = 0; i < KEY_NUM_SIZEOF; i++){
		hi318e_set_muxctrl_to_gpio_mode(keys_desc[i].gpio_desc.bank, 
								keys_desc[i].gpio_desc.pin);
		hi3518e_gpio_direction_input(keys_desc[i].gpio_desc.bank, 
								keys_desc[i].gpio_desc.pin);
		hi3518e_gpio_interrupt_config(keys_desc[i].gpio_desc.bank, 
					keys_desc[i].gpio_desc.pin, keys_desc[i].gpio_desc.irq_type);
		hi3518e_gpio_interrupt_enable(keys_desc[i].gpio_desc.bank,
								keys_desc[i].gpio_desc.pin);

		keys_desc[i].pinval = hi3518e_gpio_get(keys_desc[i].gpio_desc.bank, keys_desc[i].gpio_desc.pin);		
		
		if(keys_desc[i].pinval)
			key_val |=  1 << keys_desc[i].gpio_desc.flag;
		else
			key_val &=	~(1 << keys_desc[i].gpio_desc.flag);				
		if (ret < 0) {
				printk("hi2518e key request_irq failed \n");
				return ret;
		}						
	}
	ret = request_irq(GPIO_IRQ_NUM, hi3518e_key_irq_handler, IRQF_SHARED, "hi3518e-key", &keys_desc[0]);
	//kernel_thread(key_main_thread,NULL,CLONE_FS | CLONE_FILES | CLONE_SIGHAND);
	return 0;
}

static void __exit hi3518e_key_exit(void)
{
	free_irq(GPIO_IRQ_NUM, &keys_desc[0]);
	misc_deregister(&hi3518e_key_dev);
}

module_init(hi3518e_key_init);
module_exit(hi3518e_key_exit);

MODULE_AUTHOR("Yongbo Zhang<giraffesnn123@gmail.com>");
MODULE_DESCRIPTION("Hi3518e KEY driver");
MODULE_LICENSE("GPL");
