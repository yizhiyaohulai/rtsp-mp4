curdir = $(shell pwd)
CFLAGS += -I$(curdir)/lib/
CFLAGS += -I$(curdir)/include/
LIBS += -lpthread -lm -ldl -lmp4v2 -lstdc++
SENSOR_LIBS += $(curdir)/lib/libisp.a \
			$(curdir)/lib/libsns_ar0130.a \
			$(curdir)/lib/lib_cmoscfg.a \
			$(curdir)/lib/lib_iniparser.a \
			$(curdir)/lib/lib_hiae.a \
			$(curdir)/lib/lib_hiawb.a \
			$(curdir)/lib/lib_hiaf.a \
			$(curdir)/lib/lib_hidefog.a
MPI_LIBS += $(curdir)/lib/libmd.a  $(curdir)/lib/libive.a  $(curdir)/lib/libmpi.a
AUDIO_LIBA += $(curdir)/lib/libVoiceEngine.a $(curdir)/lib/libupvqe.a $(curdir)/lib/libdnvqe.a
all:rtsp-file
rtsp-file:
	arm-hisiv300-linux-gcc $(CFLAGS) -o rtsp-file -Dhi3518e -DHICHIP=0x3518E200 -DSENSOR_TYPE=APTINA_AR0130_DC_720P_30FPS -DHI_RELEASE -DHI_XXXX -DCHIP_ID=CHIP_HI3518E_V200 -lpthread -lm -DISP_V2 -Dhi3518ev200 -DHI_ACODEC_TYPE_INNER -mno-unaligned-access -fno-aggressive-loop-optimizations -ldl -DLCD_ILI9342  main.c ringfifo.c rtputils.c rtspservice.c rtsputils.c loadbmp.c sample_comm_audio.c sample_comm_isp.c sample_comm_sys.c  sample_comm_venc.c sample_comm_vi.c sample_comm_vo.c sample_comm_vpss.c sample_venc.c avilib.c normal.c $(INCLUDE) $(LIBS) $(MPI_LIBS) $(AUDIO_LIBA) $(SENSOR_LIBS) -L $(curdir)/lib
clean:
	rm -rfv rtsp-file

