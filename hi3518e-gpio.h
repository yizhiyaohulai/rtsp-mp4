#ifndef __HI3518E_GPIO_H__
#define __HI3518E_GPIO_H__

#define GPIO0_BASE_ADDR         	IO_ADDRESS(0x20140000)
#define MUXCTRL_REG0_BASE_ADDR		IO_ADDRESS(0x200F0000)

#define GPIO_DIR_ADDR           0x400
#define GPIO_IS_ADDR            0x404
#define GPIO_IBE_ADDR           0x408
#define GPIO_IEV_ADDR           0x40c
#define GPIO_IE_ADDR            0x410
#define GPIO_RIS_ADDR           0x414
#define GPIO_MIS_ADDR           0x418
#define GPIO_IC_ADDR            0x41c

#define GPIO_DIR(x)         ((GPIO0_BASE_ADDR + (0x10000 * x)) + GPIO_DIR_ADDR)
#define GPIO_IS(x)          ((GPIO0_BASE_ADDR + (0x10000 * x)) + GPIO_IS_ADDR)
#define GPIO_IBE(x)         ((GPIO0_BASE_ADDR + (0x10000 * x)) + GPIO_IBE_ADDR)
#define GPIO_IEV(x)         ((GPIO0_BASE_ADDR + (0x10000 * x)) + GPIO_IEV_ADDR)
#define GPIO_IE(x)          ((GPIO0_BASE_ADDR + (0x10000 * x)) + GPIO_IE_ADDR)
#define GPIO_RIS(x)         ((GPIO0_BASE_ADDR + (0x10000 * x)) + GPIO_RIS_ADDR)
#define GPIO_MIS(x)         ((GPIO0_BASE_ADDR + (0x10000 * x)) + GPIO_MIS_ADDR)
#define GPIO_IC(x)          ((GPIO0_BASE_ADDR + (0x10000 * x)) + GPIO_IC_ADDR)
#define GPIO_DATA(x, y)     ((GPIO0_BASE_ADDR + (0x10000 * x)) + (1 << (y+2)))

#define MUXCTRL_REG1_ADDR	    0x004
#define MUXCTRL_REG44_ADDR	    0x0B0
#define MUXCTRL_REG58_ADDR		0x0E8	
#define MUXCTRL_REG59_ADDR		0x0EC
#define MUXCTRL_REG60_ADDR		0x0F0
#define MUXCTRL_REG62_ADDR		0x0F8
#define MUXCTRL_REG63_ADDR		0x0FC
#define MUXCTRL_REG64_ADDR		0x100
#define MUXCTRL_REG65_ADDR		0x104

#define MUXCTRL_REG1		(MUXCTRL_REG0_BASE_ADDR + MUXCTRL_REG1_ADDR)
#define MUXCTRL_REG44	    (MUXCTRL_REG0_BASE_ADDR + MUXCTRL_REG44_ADDR)
#define MUXCTRL_REG58		(MUXCTRL_REG0_BASE_ADDR + MUXCTRL_REG58_ADDR)	
#define MUXCTRL_REG59		(MUXCTRL_REG0_BASE_ADDR + MUXCTRL_REG59_ADDR)
#define MUXCTRL_REG60		(MUXCTRL_REG0_BASE_ADDR + MUXCTRL_REG60_ADDR)
#define MUXCTRL_REG62		(MUXCTRL_REG0_BASE_ADDR + MUXCTRL_REG62_ADDR)
#define MUXCTRL_REG63		(MUXCTRL_REG0_BASE_ADDR + MUXCTRL_REG63_ADDR)
#define MUXCTRL_REG64		(MUXCTRL_REG0_BASE_ADDR + MUXCTRL_REG64_ADDR)
#define MUXCTRL_REG65		(MUXCTRL_REG0_BASE_ADDR + MUXCTRL_REG65_ADDR)

struct hi3518e_gpio_desc{
	unsigned int bank;
	unsigned int pin;
	unsigned int irq_type;
	unsigned int flag;
};

#define GPIO_IRQ_NUM	31

#endif /* __HI3518E_GPIO_H__ */
