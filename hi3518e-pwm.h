#ifndef __HI3518E_PWM_H__
#define __HI3518E_PWM_H__

#define PERI_CRG14_BASE_ADDR	IO_ADDRESS(0x20030038)
#define PWM0_BASE_ADDR			IO_ADDRESS(0x20130000)

#define PWM_CFG0_ADDR			0x0000
#define PWM_CFG1_ADDR			0x0004
#define PWM_CFG2_ADDR			0x0008
#define PWM_CTRL_ADDR			0x000C
#define PWM_STATE0_ADDR			0x0010
#define PWM_STATE1_ADDR			0x0014
#define PWM_STATE2_ADDR			0x0018

#define PWM_CFG0(ch)			((PWM0_BASE_ADDR + (0x20 * (ch))) + PWM_CFG0_ADDR)
#define PWM_CFG1(ch)			((PWM0_BASE_ADDR + (0x20 * (ch))) + PWM_CFG1_ADDR)
#define PWM_CFG2(ch)			((PWM0_BASE_ADDR + (0x20 * (ch))) + PWM_CFG2_ADDR)
#define PWM_CTRL(ch)			((PWM0_BASE_ADDR + (0x20 * (ch))) + PWM_CTRL_ADDR)
#define PWM_STATE0(ch)			((PWM0_BASE_ADDR + (0x20 * (ch))) + PWM_STATE0_ADDR)
#define PWM_STATE1(ch)			((PWM0_BASE_ADDR + (0x20 * (ch))) + PWM_STATE1_ADDR)
#define PWM_STATE2(ch)			((PWM0_BASE_ADDR + (0x20 * (ch))) + PWM_STATE2_ADDR)

struct hi3518e_pwm_desc{
	unsigned char ch;			/* PWM channel, 0 ~ 3 */ 
	unsigned int period;		/* User configured period */
	int duty;					/* User configured duty*/
	unsigned int cycles_num;	/* Number of cycles, [25:0], >=2 */
	unsigned int lvl_beats_num;	/* Number of level beats, [25:0], >= 1 */
	unsigned int num;			/* Number of square waves output by PWM */
	unsigned char inverted; 	/* PWM output control: normal mode or inverted mode */
	unsigned char keep;			/* PWM output mode: fixed number of square waves or always outputs square waves */
	unsigned char cksel;		/* PWM clock select: 3 MHz, 50 MHz, 24 MHz */
	unsigned char cken;			/* PWM clock gating register: disabled or enabled */
	unsigned char srst_req;		/* PWM soft reset request: deassert reset, reset */
};

enum hi3518e_pwm_clock{
	PWM_CLOCK_3MHZ  = 3000000,
	PWM_CLOCK_50MHZ = 50000000,
	PWM_CLOCK_24MHZ = 24000000
};

enum hi3518e_pwm_clock_sel{
	PWM_CLOCK_SEL_3MHZ  = 0x00,
	PWM_CLOCK_SEL_50MHZ = 0x01,
	PWM_CLOCK_SEL_24MHZ = 0x10
};

#define CMD_PWM_GENERATE 0X00

#define ENABLE 	1
#define DISABLE 0

#endif /* __HI3518E_PWM_H__ */
