#include "normal.h"
#include <stdio.h>
#include <dirent.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/statfs.h>
#include <fcntl.h>
#include <poll.h>

#include "hi3518e-pwm.h"
#include "hi3518e-gpio.h"

#define CMD_LED_CTRL 0x00

static DispatcherGather dispatch;

pthread_t disk_id;
pthread_t key_id;
int led_fd;
int key_fd;
int pwm_fd;
int usb_up_fd;

struct hi3518e_pwm_desc pwm2;
struct hi3518e_pwm_desc pwm3;


DispatcherGather* get_dispatch_pointer(){;
	return &dispatch;
}

void collect_dedicateddisk_space_thread(void){
	struct hi3518e_gpio_desc led;
	while(1){
		float unuse_space  = collect_disk_store_info();
		if(unuse_space < 0.05){
			led.bank =6;	
			led.pin = 5;
			led.flag = 0;	
			get_dispatch_pointer()->store_is_full = TRUE_;
			pwm2.duty = 100;
			ioctl(pwm_fd, CMD_PWM_GENERATE, &pwm2);
			printf("store space is full\n\r");
		}else{
			led.bank =6;	
			led.pin = 5;
			led.flag = 1;
			get_dispatch_pointer()->store_is_full = FALSE_;
		}
		ioctl(led_fd, CMD_LED_CTRL, &led);
		sleep(10);
	}
}

void collect_optioalkey_thread(void){
	char s;
	static struct pollfd fds[1];
	int res;
	unsigned char key_val;
	unsigned char key_status[3] = {0};
	fds[0].fd     = key_fd;
	fds[0].events = POLLIN;
	while(1){
		res= poll(fds, 1, 2000);
		if(res == 0){
			//printf("time out\n");		
			if(key_status[0] == SWITCH_PRESS){
				unsigned int pwm1_gpio_out = 0;
				key_status[0] = SWITCH_LONG;
				if(get_dispatch_pointer()->play_mode_ == PLAY_SYNCHRONIZE_MODE){
					get_dispatch_pointer()->play_mode_ = PLAY_KEY_MODE;
					//get_dispatch_pointer()->synchronize_status = FALSE_;
					key_status[0] = SWITCH_INIT;
					get_dispatch_pointer()->key_status = FALSE_;
					get_dispatch_pointer()->switch_key_sync = TRUE_;
					pwm2.duty = 100;

					HI_MPI_SYS_GetReg(0x201B03fc,&pwm1_gpio_out);
					pwm1_gpio_out |= (1 << 3);
					HI_MPI_SYS_SetReg(0x201B03fc,pwm1_gpio_out);	

					printf("long switch key mode \n\r");
				}
				else if(get_dispatch_pointer()->play_mode_ == PLAY_KEY_MODE){
					get_dispatch_pointer()->play_mode_ = PLAY_SYNCHRONIZE_MODE;
					key_status[0] = SWITCH_INIT;
					get_dispatch_pointer()->key_status = TRUE_;
					get_dispatch_pointer()->switch_key_sync = TRUE_;
					
					HI_MPI_SYS_GetReg(0x201B03fc,&pwm1_gpio_out);
					pwm1_gpio_out &= ~(1 << 3);
					printf("pwm1_gpio_out:0x%x\n\r",pwm1_gpio_out);
					HI_MPI_SYS_SetReg(0x201B03fc,pwm1_gpio_out);
					
					if(get_dispatch_pointer()->already_synch)
						pwm2.duty = 50;
					else
						pwm2.duty = 100;	
					printf("long switch synchronize mode \n\r");
				}
				ioctl(pwm_fd, CMD_PWM_GENERATE, &pwm2);
			}		
		}
		else{
			unsigned char pos = 0;
			unsigned char val = 0;
			read(key_fd, &key_val, 1);
			printf("key_status:0x%x\n\r",key_val);			
			pos = key_val & 0xf0;
			printf("pos:0x%x\n\r",pos);
			switch(pos){
				case 0x10: 
					val = key_val & 0x1;
					switch(val){
						case 0:
							get_dispatch_pointer()->already_synch &= ~(1 << 1);
							if(get_dispatch_pointer()->already_synch && get_dispatch_pointer()->play_mode_ == PLAY_SYNCHRONIZE_MODE){
								pwm2.duty	 = 50;
								ioctl(pwm_fd, CMD_PWM_GENERATE, &pwm2); 
							}
							else if(get_dispatch_pointer()->play_mode_ == PLAY_SYNCHRONIZE_MODE){
								pwm2.duty	 = 100;
								ioctl(pwm_fd, CMD_PWM_GENERATE, &pwm2); 
							}
					    break;
						case 1: 
							get_dispatch_pointer()->already_synch |= (1 << 1);
							if(get_dispatch_pointer()->already_synch && get_dispatch_pointer()->play_mode_ == PLAY_SYNCHRONIZE_MODE)
								pwm2.duty	 = 50;
							ioctl(pwm_fd, CMD_PWM_GENERATE, &pwm2);
						break;
					}
				break;
				case 0x20:
					val = (key_val >> 1) & 0x1;
					switch(val){
						case 0: key_status[1] = SWITCH_PRESS; gettimeofday(&get_dispatch_pointer()->time_start[1],NULL); break;
						case 1:
							if(key_status[1] == SWITCH_PRESS){
								struct timeval time_end;
								gettimeofday(&time_end,NULL);
                                float time_diff = (time_end.tv_sec - get_dispatch_pointer()->time_start[1].tv_sec) * 1000000 + (time_end.tv_usec - get_dispatch_pointer()->time_start[1].tv_usec);
								time_diff = time_diff * 1.0f / 1000;
								printf("time_diff:%f\n\r",time_diff);
								//if(time_diff < 50)
								//	break;
							
								key_status[1] = SWITCH_INIT;				
								if(get_dispatch_pointer()->pwm_status_ == PWM_FULL){
									get_dispatch_pointer()->pwm_status_ = PWM_HALF;
									pwm3.duty	= 50;
									get_dispatch_pointer()->already_synch |= (1 << 0);
									printf("PWM_HALF\n\r");
								}
								else if(get_dispatch_pointer()->pwm_status_ == PWM_ZERO){
									//out pwm 50
									get_dispatch_pointer()->pwm_status_ = PWM_FULL;
									pwm3.duty	= 100;
									get_dispatch_pointer()->already_synch |= (1 << 0);
									printf("PWM_FULL\n\r");
								}
								else if(get_dispatch_pointer()->pwm_status_ == PWM_HALF){
									//out pwm 100
									get_dispatch_pointer()->pwm_status_ = PWM_ZERO;
									pwm3.duty	= 0;	
									get_dispatch_pointer()->already_synch &= ~(1 << 0);
									printf("PWM_ZERO\n\r");
								}
							
								if(get_dispatch_pointer()->already_synch && get_dispatch_pointer()->play_mode_ == PLAY_SYNCHRONIZE_MODE){
									pwm2.duty = 50;
									ioctl(pwm_fd, CMD_PWM_GENERATE, &pwm2);
								}
								else if(get_dispatch_pointer()->play_mode_ == PLAY_SYNCHRONIZE_MODE){
									pwm2.duty = 100;
									ioctl(pwm_fd, CMD_PWM_GENERATE, &pwm2);
								}
								printf("pwm3 send %d\n\r",pwm3.duty);
								ioctl(pwm_fd, CMD_PWM_GENERATE, &pwm3); 
							}
						break;
					}		
				break;
				case 0x30:
					val = key_val & 0x1;
					switch(val){
						case 0:
							get_dispatch_pointer()->already_synch &= ~(1 << 1);
							if(get_dispatch_pointer()->already_synch && get_dispatch_pointer()->play_mode_ == PLAY_SYNCHRONIZE_MODE){
								pwm2.duty	 = 50;
								ioctl(pwm_fd, CMD_PWM_GENERATE, &pwm2); 
							}
							else if(get_dispatch_pointer()->play_mode_ == PLAY_SYNCHRONIZE_MODE){
								pwm2.duty	 = 100;
								ioctl(pwm_fd, CMD_PWM_GENERATE, &pwm2); 
							}
					    break;
						case 1: 
							get_dispatch_pointer()->already_synch |= (1 << 1);
							if(get_dispatch_pointer()->already_synch && get_dispatch_pointer()->play_mode_ == PLAY_SYNCHRONIZE_MODE)
								pwm2.duty	 = 50;
							ioctl(pwm_fd, CMD_PWM_GENERATE, &pwm2);
						break;
					}
					val = (key_val >> 1) & 0x1;
					switch(val){
						case 0: key_status[1] = SWITCH_PRESS; gettimeofday(&get_dispatch_pointer()->time_start[1],NULL); break;
						case 1:
							if(key_status[1] == SWITCH_PRESS){
								struct timeval time_end;
								gettimeofday(&time_end,NULL);
                                float time_diff = (time_end.tv_sec - get_dispatch_pointer()->time_start[1].tv_sec) * 1000000 + (time_end.tv_usec - get_dispatch_pointer()->time_start[1].tv_usec);
								time_diff = time_diff * 1.0f / 1000;
								//if(time_diff < 50)
								//	break;
								key_status[1] = SWITCH_INIT;				
								if(get_dispatch_pointer()->pwm_status_ == PWM_FULL){
									get_dispatch_pointer()->pwm_status_ = PWM_HALF;
									pwm3.duty	= 50;
									get_dispatch_pointer()->already_synch |= (1 << 0);
									printf("PWM_HALF\n\r");
								}
								else if(get_dispatch_pointer()->pwm_status_ == PWM_ZERO){
									//out pwm 50
									get_dispatch_pointer()->pwm_status_ = PWM_FULL;
									pwm3.duty	= 100;
									get_dispatch_pointer()->already_synch |= (1 << 0);
									printf("PWM_FULL\n\r");
								}
								else if(get_dispatch_pointer()->pwm_status_ == PWM_HALF){
									//out pwm 100
									get_dispatch_pointer()->pwm_status_ = PWM_ZERO;
									pwm3.duty	= 0;	
									get_dispatch_pointer()->already_synch &= ~(1 << 0);
									printf("PWM_ZERO\n\r");
								}
							
								if(get_dispatch_pointer()->already_synch && get_dispatch_pointer()->play_mode_ == PLAY_SYNCHRONIZE_MODE){
									pwm2.duty = 50;
									ioctl(pwm_fd, CMD_PWM_GENERATE, &pwm2);
								}
								else if(get_dispatch_pointer()->play_mode_ == PLAY_SYNCHRONIZE_MODE){
									pwm2.duty = 100;
									ioctl(pwm_fd, CMD_PWM_GENERATE, &pwm2);
								}
								printf("pwm3 send %d\n\r",pwm3.duty);
								ioctl(pwm_fd, CMD_PWM_GENERATE, &pwm3); 
							}
						break;
					}	
				break;
				case 0x40: 
					val = (key_val >>2) & 0x1;
					switch(val){
						case 0: key_status[0] = SWITCH_PRESS; gettimeofday(&get_dispatch_pointer()->time_start[2],NULL); break;
						case 1: 
							if(key_status[0] == SWITCH_PRESS){
								struct timeval time_end;
								gettimeofday(&time_end,NULL);
                                float time_diff = (time_end.tv_sec - get_dispatch_pointer()->time_start[2].tv_sec) * 1000000 + (time_end.tv_usec - get_dispatch_pointer()->time_start[2].tv_usec);
								time_diff = time_diff * 1.0f / 1000;
								if(time_diff < 50)
									break;								
								key_status[0] = SWITCH_INIT; 
								if(get_dispatch_pointer()->play_mode_ == PLAY_KEY_MODE){
									if(get_dispatch_pointer()->key_status == FALSE_){
										get_dispatch_pointer()->key_status = TRUE_;
										pwm2.duty	 = 50;
										printf("key mode and start record\n\r");
									}
									else if(get_dispatch_pointer()->key_status == TRUE_){
										get_dispatch_pointer()->key_status = FALSE_;
										pwm2.duty	 = 100;
										printf("key mode and stop record\n\r");
									}
								}  
								else if(get_dispatch_pointer()->play_mode_ == PLAY_SYNCHRONIZE_MODE){
		//							if(get_dispatch_pointer()->synchronize_status == FALSE_){
		//								get_dispatch_pointer()->synchronize_status = TRUE_;
		//								if(get_dispatch_pointer()->already_synch){
		//									pwm2.duty	 = 50;
		//								}
		//								printf("option mode and entry record\n\r");
		//							}
		//							else if(get_dispatch_pointer()->synchronize_status == TRUE_){
		//								get_dispatch_pointer()->synchronize_status = FALSE_;
		//								pwm2.duty	 = 100;
		//								printf("option mode and exit record\n\r");
		//							}
								}
							 ioctl(pwm_fd, CMD_PWM_GENERATE, &pwm2);
						}
						break;
					}
				break;
				case 0x50: 
					val = (key_val >>2) & 0x1;
					switch(val){
						case 0: key_status[0] = SWITCH_PRESS; gettimeofday(&get_dispatch_pointer()->time_start[2],NULL); break;
						case 1: 
							if(key_status[0] == SWITCH_PRESS){
								struct timeval time_end;
								gettimeofday(&time_end,NULL);
								float time_diff = (time_end.tv_sec - get_dispatch_pointer()->time_start[2].tv_sec) * 1000000 + (time_end.tv_usec - get_dispatch_pointer()->time_start[2].tv_usec);
								time_diff = time_diff * 1.0f / 1000;
								if(time_diff < 50)
									break;								
								key_status[0] = SWITCH_INIT; 
								if(get_dispatch_pointer()->play_mode_ == PLAY_KEY_MODE){
									if(get_dispatch_pointer()->key_status == FALSE_){
										get_dispatch_pointer()->key_status = TRUE_;
										pwm2.duty	 = 50;
										printf("key mode and start record\n\r");
									}
									else if(get_dispatch_pointer()->key_status == TRUE_){
										get_dispatch_pointer()->key_status = FALSE_;
										pwm2.duty	 = 100;
										printf("key mode and stop record\n\r");
									}
								}  
								else if(get_dispatch_pointer()->play_mode_ == PLAY_SYNCHRONIZE_MODE){
		//							if(get_dispatch_pointer()->synchronize_status == FALSE_){
		//								get_dispatch_pointer()->synchronize_status = TRUE_;
		//								if(get_dispatch_pointer()->already_synch){
		//									pwm2.duty	 = 50;
		//								}
		//								printf("option mode and entry record\n\r");
		//							}
		//							else if(get_dispatch_pointer()->synchronize_status == TRUE_){
		//								get_dispatch_pointer()->synchronize_status = FALSE_;
		//								pwm2.duty	 = 100;
		//								printf("option mode and exit record\n\r");
		//							}
								}
							 ioctl(pwm_fd, CMD_PWM_GENERATE, &pwm2);
						}
						break;
					}
				break;
				case 0x60: 
					val = (key_val >>2) & 0x1;
					switch(val){
						case 0: key_status[0] = SWITCH_PRESS; gettimeofday(&get_dispatch_pointer()->time_start[2],NULL); break;
						case 1: 
							if(key_status[0] == SWITCH_PRESS){
								struct timeval time_end;
								gettimeofday(&time_end,NULL);
								float time_diff = (time_end.tv_sec - get_dispatch_pointer()->time_start[2].tv_sec) * 1000000 + (time_end.tv_usec - get_dispatch_pointer()->time_start[2].tv_usec);
								time_diff = time_diff * 1.0f / 1000;
								if(time_diff < 50)
									break;								
								key_status[0] = SWITCH_INIT; 
								if(get_dispatch_pointer()->play_mode_ == PLAY_KEY_MODE){
									if(get_dispatch_pointer()->key_status == FALSE_){
										get_dispatch_pointer()->key_status = TRUE_;
										pwm2.duty	 = 50;
										printf("key mode and start record\n\r");
									}
									else if(get_dispatch_pointer()->key_status == TRUE_){
										get_dispatch_pointer()->key_status = FALSE_;
										pwm2.duty	 = 100;
										printf("key mode and stop record\n\r");
									}
								}  
								else if(get_dispatch_pointer()->play_mode_ == PLAY_SYNCHRONIZE_MODE){
		//							if(get_dispatch_pointer()->synchronize_status == FALSE_){
		//								get_dispatch_pointer()->synchronize_status = TRUE_;
		//								if(get_dispatch_pointer()->already_synch){
		//									pwm2.duty	 = 50;
		//								}
		//								printf("option mode and entry record\n\r");
		//							}
		//							else if(get_dispatch_pointer()->synchronize_status == TRUE_){
		//								get_dispatch_pointer()->synchronize_status = FALSE_;
		//								pwm2.duty	 = 100;
		//								printf("option mode and exit record\n\r");
		//							}
								}
							 ioctl(pwm_fd, CMD_PWM_GENERATE, &pwm2);
						}
						break;
					}
				break;
				case 0x70: 
					val = (key_val >>2) & 0x1;
					switch(val){
						case 0: key_status[0] = SWITCH_PRESS; gettimeofday(&get_dispatch_pointer()->time_start[2],NULL); break;
						case 1: 
							if(key_status[0] == SWITCH_PRESS){
								struct timeval time_end;
								gettimeofday(&time_end,NULL);
								float time_diff = (time_end.tv_sec - get_dispatch_pointer()->time_start[2].tv_sec) * 1000000 + (time_end.tv_usec - get_dispatch_pointer()->time_start[2].tv_usec);
								time_diff = time_diff * 1.0f / 1000;
								if(time_diff < 50)
									break;								
								key_status[0] = SWITCH_INIT; 
								if(get_dispatch_pointer()->play_mode_ == PLAY_KEY_MODE){
									if(get_dispatch_pointer()->key_status == FALSE_){
										get_dispatch_pointer()->key_status = TRUE_;
										pwm2.duty	 = 50;
										printf("key mode and start record\n\r");
									}
									else if(get_dispatch_pointer()->key_status == TRUE_){
										get_dispatch_pointer()->key_status = FALSE_;
										pwm2.duty	 = 100;
										printf("key mode and stop record\n\r");
									}
								}  
								else if(get_dispatch_pointer()->play_mode_ == PLAY_SYNCHRONIZE_MODE){
		//							if(get_dispatch_pointer()->synchronize_status == FALSE_){
		//								get_dispatch_pointer()->synchronize_status = TRUE_;
		//								if(get_dispatch_pointer()->already_synch){
		//									pwm2.duty	 = 50;
		//								}
		//								printf("option mode and entry record\n\r");
		//							}
		//							else if(get_dispatch_pointer()->synchronize_status == TRUE_){
		//								get_dispatch_pointer()->synchronize_status = FALSE_;
		//								pwm2.duty	 = 100;
		//								printf("option mode and exit record\n\r");
		//							}
								}
							 ioctl(pwm_fd, CMD_PWM_GENERATE, &pwm2);
						}
						break;
					}
				break;	
			}			
		}	
	}
}
void dispather_schedule(){
	unsigned int pwm1_gpio_mode = 1;
	unsigned int pwm1_gpio_dir = 0;
	unsigned int pwm1_gpio_out = 0;

	get_dispatch_pointer()->play_mode_ = PLAY_SYNCHRONIZE_MODE;

	led_fd = open("/dev/hi3518e-led", O_RDWR);
	key_fd = open("/dev/hi3518e-key", O_RDWR | O_NONBLOCK);
	pwm_fd = open("/dev/hi3518e-pwm", O_RDWR);
	usb_up_fd = open("/dev/opticalswitch",O_RDWR);
	if (key_fd < 0 || pwm_fd < 0 || led_fd < 0){
		perror("open hision key driver failure\n\r");
		exit(0);
	}

	if (usb_up_fd < 0){
		perror("open usb update driver failure\n\r");
		exit(0);
	}


	pwm2.ch = 2;
	pwm2.inverted = DISABLE;
	pwm2.keep     = ENABLE;
	pwm2.num		 = 1;
	pwm2.period	 = 2;
	pwm2.duty	 = 100;
	ioctl(pwm_fd, CMD_PWM_GENERATE, &pwm2);

	HI_MPI_SYS_SetReg(0x200f00ec,pwm1_gpio_mode);
	
	HI_MPI_SYS_GetReg(0x201B0400,&pwm1_gpio_dir);
	pwm1_gpio_dir |= 1 << 3;
	HI_MPI_SYS_SetReg(0x201B0400,pwm1_gpio_dir);

	HI_MPI_SYS_GetReg(0x201B03fc,&pwm1_gpio_out);
	pwm1_gpio_out &= ~(1 << 3);
	HI_MPI_SYS_SetReg(0x201B03fc,pwm1_gpio_out);	


	pwm3.ch = 3;
	pwm3.inverted = DISABLE;
	pwm3.keep	  = ENABLE;
	pwm3.num	  = 1;
	pwm3.period	  = 10;

	unsigned int test_gpio_val;
	HI_MPI_SYS_GetReg(0x201b03fc,&test_gpio_val);
	if(test_gpio_val & 0x02) //高电平
	{
		get_dispatch_pointer()->already_synch |= (1 << 1);
		if(get_dispatch_pointer()->already_synch && get_dispatch_pointer()->play_mode_ == PLAY_SYNCHRONIZE_MODE)
			pwm2.duty	 = 50;
		ioctl(pwm_fd, CMD_PWM_GENERATE, &pwm2);
	}
	else{	//低电平			
		get_dispatch_pointer()->already_synch &= ~(1 << 1);
		if(get_dispatch_pointer()->already_synch && get_dispatch_pointer()->play_mode_ == PLAY_SYNCHRONIZE_MODE){
			pwm2.duty	 = 50;
			ioctl(pwm_fd, CMD_PWM_GENERATE, &pwm2); 
		}
		else if(get_dispatch_pointer()->play_mode_ == PLAY_SYNCHRONIZE_MODE){
			pwm2.duty	 = 100;
			ioctl(pwm_fd, CMD_PWM_GENERATE, &pwm2); 
		}
	}
	pthread_create(&disk_id,NULL,collect_dedicateddisk_space_thread,NULL);
	pthread_create(&key_id,NULL,collect_optioalkey_thread,NULL);
}

int customMP4Filter(const struct dirent *pDir)
{
    if (strncmp("MP4_", pDir->d_name, 4) == 0
            && pDir->d_type & DT_REG)
    {
        if (strstr(pDir->d_name, ".mp4") == NULL)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
    return 0;
}

int customAVIFilter(const struct dirent *pDir)
{
    if (strncmp("AVI_", pDir->d_name, 4) == 0
            && pDir->d_type & DT_REG)
    {
        if (strstr(pDir->d_name, ".avi") == NULL)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
    return 0;
}

int analyze_currentpath_maxid(char* path){

    struct dirent **namelist;
    int n;
    int i;
    n = scandir(path, &namelist, customMP4Filter, alphasort);
    if (n < 0)
    {
        perror("scandir");
    }
    else
    {
        int val = 0;
        for (i = 0; i < n; i++)
        {
            char dest[6] = {""};
            strncpy(dest, namelist[i]->d_name + 4, 5);
            val = atoi(dest);
            free(namelist[i]);
        }
        free(namelist);
        return val;
    }
    return -1;
}


int analyze_avipath_maxid(char* path){

    struct dirent **namelist;
    int n;
    int i;
    n = scandir(path, &namelist, customAVIFilter, alphasort);
    if (n < 0)
    {
        perror("scandir");
    }
    else
    {
        int val = 0;
        for (i = 0; i < n; i++)
        {
            char dest[6] = {""};
            strncpy(dest, namelist[i]->d_name + 4, 5);
            val = atoi(dest);
            free(namelist[i]);
        }
        free(namelist);
        return val;
    }
    return -1;
}

float collect_disk_store_info(){
	struct statfs disk_info;

	statfs("/usr/mmc",&disk_info);
	//uint64_t blocksize = disk_info.f_bsize;
	uint64_t totalsize = disk_info.f_blocks;

	uint64_t freedisk = disk_info.f_bfree;
	uint64_t availabledisk = disk_info.f_bavail;

//	printf("freedisk:%d\n\r",disk_info.f_bfree);
//	printf("availabledisk:%d\n\r",disk_info.f_bavail);
//	printf("totaldisk:%d\n\r",disk_info.f_blocks);

	return availabledisk * 1.0 /totalsize;
}

void usb_update_mp4_file(){
	int status = 1;
	int channel = 9;
	ioctl(usb_up_fd,status,channel);
}

